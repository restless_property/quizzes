"""
A utilitarian module
used to make unique UUID sequences.
"""

import uuid


def generate_uuid():
    result = uuid.uuid4()
    return result
