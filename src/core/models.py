"""
Module 'models' from the app. "core".
"""
from django.db import models
from datetime import datetime

# Create your models here.


class BaseModel(models.Model):

    class Meta:
        abstract = True

    create_date = models.DateTimeField(null=True, auto_now_add=True)
    write_date = models.DateTimeField(null=True, auto_now=True)

    def format_create_date(self):
        return self.create_date.strftime('%x %X')

    def format_write_date(self):
        return self.write_date.strftime('%x %X')

    def show_now(self):
        return datetime.now().strftime('%x %X')
