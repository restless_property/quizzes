"""
Module 'forms' for the app 'accounts'.
"""

from django.contrib.auth.forms import PasswordChangeForm, ReadOnlyPasswordHashField, UserChangeForm, UserCreationForm
from django.urls import reverse_lazy

from accounts.models import User


class AccountRegistrationForm(UserCreationForm):
    """
    The class creates an HTML-form to register a new user.
    """

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']


class AccountUpdateForm(UserChangeForm):
    """
    The class creates an HTML-form to edit information about a user.
    """
    link = reverse_lazy('accounts:password')  # 'profile/password'

    class Meta(UserChangeForm.Meta):
        fields = ['username', 'first_name', 'last_name', 'email']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['password'] = ReadOnlyPasswordHashField(
            label="Password",
            help_text='Raw passwords are not visible. You can change the password using '
                      f'<a href="{self.link}">THIS FORM</a>.')


class AccountPasswordUpdateForm(PasswordChangeForm):
    """
    The class creates an HTML-form to edit information about a user.
    """
