"""
Module 'models' from the app. "Accounts".
"""

from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.


class User(AbstractUser):

    class Meta:
        permissions = [('view_stats', 'Check statistics')]

    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    rating = models.SmallIntegerField(default=0)
