"""
The module contains view-functions for the App. "accounts".
"""
from django.views.generic.list import MultipleObjectMixin

from accounts.forms import AccountPasswordUpdateForm, AccountRegistrationForm, AccountUpdateForm

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.shortcuts import render  # noqa
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, UpdateView, ListView
from django.views.generic.edit import DeleteView

from accounts.models import User

# Create your views here.


class AccountRegistrationView(CreateView):
    """
    The class operates the process of the registration of a new user.
    """
    model = User
    template_name = 'accounts/registration.html'
    success_url = reverse_lazy('accounts:login')
    form_class = AccountRegistrationForm

    def form_valid(self, form):
        """
        Modifies the displayed massage that confirms a successful registration.
        """
        result = super().form_valid(form)
        messages.success(self.request, 'A new user has been successfully registered.')
        return result


class AccountLoginView(LoginView):
    """
    The class operates the process of the validation of a user.
    """
    template_name = 'accounts/login.html'
    success_url = reverse_lazy('index')

    def get_redirect_url(self):
        """
        Defines the URL to open after the login.
        """
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('index')

    def form_valid(self, form):
        """
        Modifies the displayed massage that confirms a successful login.
        """
        result = super().form_valid(form)
        messages.success(self.request, f'The user <<{self.request.user}>> has logged in successfully.')
        return result


class AccountLogoutView(LogoutView):
    """
    The class operates the process of the validation of a user.
    """
    template_name = 'accounts/logout.html'

    def get_context_data(self, **kwargs):
        messages.success(self.request, 'You have been logged out successfully.')
        return super().get_context_data(**kwargs)


class AccountUpdateView(MultipleObjectMixin, UpdateView):
    """
    The class processes the edit of user's data.
    """
    model = User
    template_name = 'accounts/profile.html'
    success_url = reverse_lazy('index')
    form_class = AccountUpdateForm
    paginate_by = 5

    def get_object(self, queryset=None):
        return self.request.user

    def get_context_data(self):
        context = super().get_context_data(object_list=self.get_queryset())
        return context

    def get_queryset(self):
        return self.object.results.all().select_related('user')

    def form_valid(self, form):
        """
        Modifies the displayed massage that confirms a successful profile update.
        """
        result = super().form_valid(form)
        messages.success(self.request, f'The user <<{self.request.user}>> has been successfully updated.')
        return result


class AccountPasswordChangeView(LoginRequiredMixin, PasswordChangeView):
    """
    Change the account password.
    """
    model = User
    template_name = 'accounts/password.html'
    success_url = reverse_lazy('index')
    form_class = AccountPasswordUpdateForm

    def form_valid(self, form):
        """
        Modifies the displayed massage that confirms a successful password alteration.
        """
        result = super().form_valid(form)
        messages.success(self.request, f'The user <<{self.request.user}>> has successfully changed the password.')
        return result


class AccountDeleteView(LoginRequiredMixin, DeleteView):
    """
    Delete an Account.
    """

    model = User
    success_url = reverse_lazy('index')
    template_name = 'accounts/delete.html'

    def get_context_data(self, **kwargs):
        messages.success(self.request, 'Your account has been successfully deleted.')
        return super().get_context_data(**kwargs)


class UserRatingView(LoginRequiredMixin, ListView):
    model = User
    template_name = 'user/rating.html'
    login_url = reverse_lazy('accounts:login')
    paginate_by = 20

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['users'] = self.model.objects.all().order_by('-rating', 'username').exclude(rating=0)
        # context['students'] = self.get_filter().qs.select_related('group', 'led_group')  # replaced to get_qs()
        return context
