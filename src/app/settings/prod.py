from app.settings.base import *  # noqa

DEBUG = False

MEDIA_ROOT = '/var/www/quiz/media'
STATIC_ROOT = '/var/www/quiz/static'
