from django import template

register = template.Library()


def negate(value):
    return -value


def multiply(value, arg):
    return value * arg


def divide(value, arg):
    return value / arg


# @register.simple_tag(name='expression')
def expression(value, *args):
    for idx, arg in enumerate(args, 1):
        value = value.replace(f'%{idx}', str(arg))
    return eval(value)


register.filter('negate', negate)
register.filter('multiply', multiply)
register.filter('divide', divide)
register.simple_tag(name='expression', func=expression)
