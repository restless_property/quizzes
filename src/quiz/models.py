"""
The module 'models' from the app. "quiz".
"""
import datetime

from django.db import models
from django.core.validators import MaxValueValidator
from django.utils.functional import cached_property

from accounts.models import User

# Create your models here.
from core.models import BaseModel
from core.utils import generate_uuid


class Test(BaseModel):
    QUESTION_MIN_LIMIT = 3
    QUESTION_MAX_LIMIT = 20

    class LEVEL_CHOICES(models.IntegerChoices):
        BASIC = 0, "Basic"
        MIDDLE = 1, "Middle"
        ADVANCED = 2, "Advanced"

    # topic = models.ForeignKey(to=Topic, related_name='tests', null=True, on_delete=models.SET_NULL)
    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)
    title = models.CharField(max_length=64)
    description = models.TextField(max_length=1024, null=True, blank=True)
    level = models.PositiveSmallIntegerField(choices=LEVEL_CHOICES.choices, default=LEVEL_CHOICES.MIDDLE)
    image = models.ImageField(default='default.png', upload_to='covers')

    def questions_count(self):
        return self.questions.count()

    @cached_property  # the function is used 3 times in this module
    def defy_user_to_score_to_time(self):
        """
        Defy the user who scored the best result of a test
        in correlation to the time of the completion of the test.

        :return : (dict) a data structure with the sought-for values
                         (best_result, best_user, time_best)

        Local variables:
        results: (QS) instances of the model 'Result'
                      in relation to a specific test
                      as an instance of the class 'Test'
        user_to_score_to_time_list: (list) a list of tuples
                                           containing data on each
                                           result of a completed test
        result: (Result) instance of the model 'Result' in the QS
                         that contains details on results of tests
        scores: (list) scores in tests received by users
                       who have completed them
        user_to_score_to_time: (tuple) information about one result
                                in the list of test results (user_to_score_to_time_list)
                                that includes the user, the score and the time the test was finished
        """
        results = self.results.all()
        user_to_score_to_time_list = []
        for result in results:
            user_to_score_to_time_list.append((result.user, result.num_correct_answers,
                                              (result.write_date - result.create_date)))
        scores = [user_to_score_to_time[1] for user_to_score_to_time in user_to_score_to_time_list]
        best_result = max(scores) if scores else 0  # in case there have been no results yielded yet
        best_user, time_best = None, None     # in case there have been no results yielded yet
        for user_to_score_to_time in user_to_score_to_time_list:
            if user_to_score_to_time[1] == best_result:
                best_user = user_to_score_to_time[0]  # the latter score overrides 'best_user'
                time_best = user_to_score_to_time[2]  # the latter score overrides 'time_best'
        return {'best_result': best_result,
                'best_user': best_user,
                'time_best': (time_best - datetime.timedelta(0, 0, time_best.microseconds)) if time_best else 0
                }

    def get_best_result(self):  # defy_user_to_score_to_time becomes the result of its execution when @cached
        score_user_date = self.defy_user_to_score_to_time
        return f'{score_user_date["best_result"]}/{self.questions_count()}'

    def get_best_user(self):  # defy_user_to_score_to_time becomes the result of its execution when @cached
        score_user_date = self.defy_user_to_score_to_time
        return score_user_date['best_user']

    def get_time_of_best_score(self):  # a function becomes the result of its execution when @cached
        score_user_date = self.defy_user_to_score_to_time
        return score_user_date['time_best']

    def __str__(self):
        return f'{self.title}'


class Question(models.Model):
    ANSWER_MIN_LIMIT = 3
    ANSWER_MAX_LIMIT = 6

    test = models.ForeignKey(to=Test, related_name='questions', on_delete=models.CASCADE)
    order_number = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(Test.QUESTION_MAX_LIMIT)])
    text = models.CharField(max_length=64)

    def __str__(self):
        return f'{self.text}'


class Choice(models.Model):
    text = models.CharField(max_length=64)
    question = models.ForeignKey(to=Question, related_name='choices', on_delete=models.CASCADE)
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.text}'


class Result(BaseModel):
    class STATE(models.IntegerChoices):
        NEW = 0, "New"
        FINISHED = 1, "Finished"

    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)
    user = models.ForeignKey(to=User, related_name='results', on_delete=models.CASCADE)
    test = models.ForeignKey(to=Test, related_name='results', on_delete=models.CASCADE)
    state = models.PositiveSmallIntegerField(default=STATE.NEW, choices=STATE.choices)
    current_order_number = models.PositiveSmallIntegerField(null=True)

    num_correct_answers = models.PositiveSmallIntegerField(
        default=0,
        validators=[
            MaxValueValidator(Test.QUESTION_MAX_LIMIT)
        ]
    )
    num_incorrect_answers = models.PositiveSmallIntegerField(
        default=0,
        validators=[
            MaxValueValidator(Test.QUESTION_MAX_LIMIT)
        ]
    )

    def update_result(self, order_number, question, selected_choices):
        correct_choices = [
            choice.is_correct
            for choice in question.choices.all()
        ]

        correct_answer = True
        for z in zip(selected_choices, correct_choices):
            correct_answer &= (z[0] == z[1])

        # TODO: if BackSpace is clicked in the Browser, 'num_*_answers' is incremented from each 'selected_choices'
        self.num_correct_answers += int(correct_answer)
        self.num_incorrect_answers += (1 - int(correct_answer))
        self.current_order_number = order_number
        if order_number == question.test.questions_count():
            self.state = self.STATE.FINISHED
            user = User.objects.get(id=self.user.id)
            user.rating += self.points()
            user.save()
        self.save()

    def points(self):
        return max(0, self.num_correct_answers - self.num_incorrect_answers)

    def calculate_success_rate(self):  # whithout the 1st 'round' the result may be 1/3 * 100 --> 33.300000000000004
        return f'{round(round(self.num_correct_answers / self.test.questions_count(), ndigits=3) * 100, 1)} %'

    def get_time_of_test(self):
        test_time = self.write_date - self.create_date
        return test_time - datetime.timedelta(0, 0, test_time.microseconds)
