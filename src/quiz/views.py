"""
Model 'views' from the app. 'quiz'.
"""
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import HttpResponseRedirect, HttpResponse  # noqa
from django.shortcuts import render, get_object_or_404  # noqa
from django.urls import reverse_lazy, reverse
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.views.generic.list import MultipleObjectMixin

from accounts.models import User
from quiz.forms import ChoiceFormSet
from quiz.models import Test, Result, Question


class TestListView(LoginRequiredMixin, ListView):
    """
    View the list of tests.
    """
    model = Test
    template_name = 'tests/list.html'
    context_object_name = 'tests'
    login_url = reverse_lazy('accounts:login')


class TestDetailsView(LoginRequiredMixin, DetailView, MultipleObjectMixin):
    """
    View the Result of a Test.
    """
    model = Test
    template_name = 'tests/details.html'
    pk_url_kwarg = 'uuid'
    paginate_by = 5

    def get_object(self):
        uuid = self.kwargs.get('uuid')
        return self.model.objects.get(uuid=uuid)

    def get_context_data(self, **kwargs):  # Optimization?
        context = super().get_context_data(object_list=self.get_queryset().select_related('user'), **kwargs)
        # context['result_list'] = Result.objects.filter(
        #     test=self.get_object(),
        #     user=self.request.user
        # ).order_by('state')
        return context

    def get_queryset(self):
        return Result.objects.filter(
            test=self.get_object(),
            user=self.request.user
        ).order_by('state')


class TestResultDetailsView(LoginRequiredMixin, DetailView):
    model = Result
    template_name = 'results/details.html'
    context_object_name = 'result'
    pk_url_kwarg = 'uuid'
    login_url = reverse_lazy('accounts:login')

    def get_object(self):
        uuid = self.kwargs.get('result_uuid')
        return self.get_queryset().get(uuid=uuid)


class TestResultCreateView(LoginRequiredMixin, CreateView):
    """
    Create a Result.
    """
    login_url = reverse_lazy('accounts:login')

    def post(self, request, uuid):
        result = Result.objects.create(
            test=Test.objects.get(uuid=uuid),  # High-level 'get' filter; DisADV: extra-SQL-requests
            user=request.user,
            state=Result.STATE.NEW,
            current_order_number=0
        )
        result.save()

        return HttpResponseRedirect(reverse('tests:question',
                                            kwargs={
                                                'uuid': uuid,
                                                'result_uuid': result.uuid,
                                                # 'order_number': 1  # special validation required (Qu-ns start from 1)
                                                 }
                                            ))


class TestResultUpdateView(LoginRequiredMixin, UpdateView):
    """
    Update a Result.
    """
    login_url = reverse_lazy('accounts:login')

    def get(self, request, uuid, result_uuid):
        # result = Result.objects.get(uuid=result_uuid)
        return HttpResponseRedirect(reverse('tests:question',
                                            kwargs={
                                                'uuid': uuid,
                                                'result_uuid': result_uuid,
                                                # 'order_number': result.current_order_number+1
                                            }
                                            ))


class TestResultDeleteView(LoginRequiredMixin, DeleteView):
    """
    Delete a Result.
    """
    model = Result
    template_name = 'results/delete.html'
    context_object_name = 'result'
    success_url = reverse_lazy('tests:details')
    pk_url_kwarg = 'uuid'

    def get_object(self):
        uuid = self.kwargs.get('result_uuid')
        return self.model.objects.get(uuid=uuid)

    def get_success_url(self):
        uuid = self.kwargs.get('uuid')
        return reverse('tests:details', args=(uuid,))


class TestResultQuestionView(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('accounts:login')

    def get(self, request, uuid, result_uuid):
        order_number = Result.objects.get(uuid=result_uuid).current_order_number+1
        question = Question.objects.get(
            test__uuid=uuid,  # Low-level 'get' filter (with 'suffixes'); ADV: no extra-SQL-requests
            order_number=order_number
        )

        choices = ChoiceFormSet(queryset=question.choices.all())
        return render(request=request,
                      template_name='tests/question.html',
                      context={
                          'question': question,
                          'choices': choices
                           }
                      )

    def post(self, request, uuid, result_uuid):
        order_number = Result.objects.get(uuid=result_uuid).current_order_number+1

        question = Question.objects.get(
            test__uuid=uuid,
            order_number=order_number
        )

        choices = ChoiceFormSet(data=request.POST)

        selected_choices = [
            'is_selected' in form.changed_data
            for form in choices.forms
            ]

        result = Result.objects.get(
            uuid=result_uuid
            )

        # ------- Computation of correct answers  (inside the method of the model) ------- #
        if selected_choices.count(True) == 1:
            result.update_result(order_number, question, selected_choices)
        else:
            messages.info(self.request, 'Please, select one checkbox to proceed.')
            order_number -= 1
        # ------- ---------- ------- #

        if result.state == Result.STATE.FINISHED:
            return HttpResponseRedirect(reverse('tests:result_details',
                                                kwargs={
                                                    'uuid': uuid,
                                                    'result_uuid': result.uuid
                                                    }
                                                ))
        else:
            return HttpResponseRedirect(reverse('tests:question',
                                                kwargs={
                                                    'uuid': uuid,
                                                    'result_uuid': result.uuid,
                                                    # 'order_number': order_number + 1
                                                }
                                                ))


class LeaderboardView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    permission_required = ['accounts.view_stats']
    model = User
    # template = 'leaderboard.html'
    # context_object_name = 'users'

    def get(self):
        return HttpResponse('WORKABLE OUTPUT')
