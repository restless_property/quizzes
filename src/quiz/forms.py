"""
Module 'forms' from the app. 'quiz'.
"""

from django.core.exceptions import ValidationError
from django.forms import BaseInlineFormSet, ModelForm, modelformset_factory
from django import forms

from quiz.models import Choice


class QuestionsInlineFormSet(BaseInlineFormSet):

    def clean(self):
        if not (self.instance.QUESTION_MIN_LIMIT <= len(self.forms) <= self.instance.QUESTION_MAX_LIMIT):
            raise ValidationError('The quantity of questions must be between {} and {}'.format(
                self.instance.QUESTION_MIN_LIMIT, self.instance.QUESTION_MAX_LIMIT
            ))

        if not self.cleaned_data[0].get('order_number') == 1:
            raise ValidationError('The numeration of questions must start from "1"')

        increment = 0
        for form in self.forms:
            if not self.cleaned_data[0+increment].get('order_number') == 1+increment:
                raise ValidationError('The numeration of questions must be consequent')
            increment += 1


class ChoiceInlineFormSet(BaseInlineFormSet):

    def clean(self):
        if not (self.instance.ANSWER_MIN_LIMIT <= len(self.forms) <= self.instance.ANSWER_MAX_LIMIT):
            raise ValidationError('The quantity of answers must be between {} and {}'.format(
                self.instance.ANSWER_MIN_LIMIT, self.instance.ANSWER_MAX_LIMIT
            ))

        total_number = sum(
            1
            for form in self.forms
            if form.cleaned_data['is_correct']
        )

        if total_number == len(self.forms):
            raise ValidationError('NOT allowed to select every choice')

        if total_number == 0:
            raise ValidationError('Select at least 1 option')


class ChoiceForm(ModelForm):
    is_selected = forms.BooleanField(required=False)

    class Meta:
        model = Choice
        fields = ['text']

    # def __init__(self, **kwargs):
    #     super().__init__(**kwargs)
        # self.fields['text'].widget = HiddenInput()


ChoiceFormSet = modelformset_factory(
    model=Choice,
    form=ChoiceForm,
    extra=0
)
