#!/bin/bash

pip install psycopg2

python src/manage.py runserver --settings=app.settings.${MODE} 0:${WSGI_PORT}
